# Название приложения

### Описание

Данное приложение на Spring Boot 3.2.0 и Java 17 предоставляет REST API для работы с ресурсами "Посты". Оно обращается к API [https://jsonplaceholder.typicode.com/](https://jsonplaceholder.typicode.com/) для получения данных о постах. В приложении реализована возможность получать, создавать, удалять и изменять посты. Весь API задокументирован в Swagger.

### Инструкция по установке

1. Убедитесь, что у вас установлен Java 17 и Docker.
2. Скачайте исходные файлы приложения.
3. Перейдите в корневую папку приложения.
4. Соберите Docker:
```shell
docker compose up
```
5. После этого приложение будет доступно по адресу [http://localhost:8080](http://localhost:8080).

### API документация

API приложения полностью задокументировано в Swagger. После запуска приложения, вы можете открыть Swagger UI по адресу [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html), чтобы ознакомиться с документацией и протестировать эндпоинты.

#### Эндпоинты

- `GET /posts` - Получить все посты. Возвращает список всех постов.
- `GET /posts/{id}` - Получить пост по ID. Возвращает информацию о посте с указанным ID.
- `POST /posts` - Создать пост. Создает новый пост.
- `PUT /posts/{id}` - Изменить пост. Изменяет пост с указанным ID.
- `DELETE /posts/{id}` - Удалить пост. Удаляет пост с указанным ID.

### Автор

- Семенов Владислав Вячеславович.
