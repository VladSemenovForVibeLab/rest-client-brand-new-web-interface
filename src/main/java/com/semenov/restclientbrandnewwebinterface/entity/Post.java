package com.semenov.restclientbrandnewwebinterface.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Post {
    @Schema(description = "userId", example = "1")
    private Integer userId;
    @Schema(description = "id", example = "1")
    private Integer id;
    @Schema(description = "title", example = "qui est esse")
    private String title;
    @Schema(description = "body", example = "est rerum tempore vitae\\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\\nqui aperiam non debitis possimus qui neque nisi nulla")
    private String body;
}
