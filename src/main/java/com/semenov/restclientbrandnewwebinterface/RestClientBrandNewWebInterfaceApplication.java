package com.semenov.restclientbrandnewwebinterface;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestClientBrandNewWebInterfaceApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestClientBrandNewWebInterfaceApplication.class, args);
    }

}
