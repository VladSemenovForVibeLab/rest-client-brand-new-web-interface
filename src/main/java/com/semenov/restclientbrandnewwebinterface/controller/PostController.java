package com.semenov.restclientbrandnewwebinterface.controller;

import com.semenov.restclientbrandnewwebinterface.entity.Post;
import com.semenov.restclientbrandnewwebinterface.service.PostService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Tag(name = "Post Controller",description = "Post API -> https://jsonplaceholder.typicode.com/")
public class PostController {
    @Autowired
    private PostService postService;
    @GetMapping("/listofposts")
    @Operation(summary = "Get Posts")
    List<Post> getPosts(){
        return postService.getPostList();
    }
    @GetMapping("/getPostById/{id}")
    @Operation(summary = "Get Post By Id")
    Post getPostById(@PathVariable Integer id){
        return postService.getPostById(id);
    }
    @PostMapping("/insertPost")
    @Operation(summary = "Insert Post")
    Post insert(@RequestBody Post post){
        return postService.insert(post);
    }
    @PutMapping("/update/{id}")
    @Operation(summary = "Update Post")
    Post update(@PathVariable Integer id
                ,@RequestBody Post post){
        return postService.update(id,post);
    }
    @DeleteMapping("/delete/{id}")
    @Operation(summary = "Delete Post By Id")
    void delete(@PathVariable Integer id){
        postService.delete(id);
    }
}
